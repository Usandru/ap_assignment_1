module SubsInterpreter
       (
         Value(..)
       , runExpr
       -- You may include additional exports here, if you want to
       -- write unit tests for them.
       , structEquals
       , subtractVal
       , multiply
       , modulo
       , lessThan
       , addOrConcat
       )
       where

import SubsAst

-- You might need the following imports
import Control.Monad
import qualified Data.Map as Map
import Data.Map(Map)

import Data.Either

-- | A value is either an integer, the special constant undefined,
--   true, false, a string, or an array of values.
-- Expressions are evaluated to values.
data Value = IntVal Int
           | UndefinedVal
           | TrueVal | FalseVal
           | StringVal String
           | ArrayVal [Value]
           deriving (Eq, Show)


type Error = String
type Env = Map Ident Value
type Primitive = [Value] -> Either Error Value
type PEnv = Map FunName Primitive
type Context = (Env, PEnv)

initialContext :: Context
initialContext = (Map.empty, initialPEnv)
  where initialPEnv =
          Map.fromList [ ("===", structEquals)
                       , ("<", lessThan)
                       , ("+", addOrConcat)
                       , ("*", multiply)
                       , ("-", subtractVal)
                       , ("%", modulo)
                       , ("Array", mkArray)
                       ]


newtype SubsM a = SubsM {runSubsM :: Context -> Either Error (a, Env)}

instance Monad SubsM where
  return x = SubsM $ \c -> Right (x, fst c)
  m >>= f = SubsM $ \c -> let a = runSubsM m c
                           in case a of
                                   Left s -> fail s
                                   Right (v, env) -> runSubsM (f v)
                                                              (env, snd c)
  fail s = SubsM $ \_ -> Left s

-- retrieves the current environment. Best used in do-blocks
getEnv :: SubsM Env
getEnv = SubsM $ \c -> Right (fst c, fst c)
-- retrieves the Primitive environment, which cannot be modified
getPEnv :: SubsM PEnv
getPEnv = SubsM $ \c -> Right (snd c, fst c)
-- replaces the current environment with a new one
put :: Env -> SubsM ()
put env = SubsM $ \_ -> Right ((), env)

-- You may modify these if you want, but it shouldn't be necessary
instance Functor SubsM where
  fmap = liftM
instance Applicative SubsM where
  pure = return
  (<*>) = ap

mkArray :: Primitive
mkArray [IntVal n] | n >= 0 = return $ ArrayVal (replicate n UndefinedVal)
mkArray _ = Left "Array() called with wrong number or type of arguments"

-- Implements structural equality for all SubScript values,
-- including UndefinedVal.
structEquals :: Primitive
structEquals [x, y] = if x == y then Right TrueVal
                                else Right FalseVal
structEquals _ = Left structOpErr

-- Error message for structEquals
structOpErr = "Operator '===' called with wrong number of arguments"

-- Implements comparison for IntVal and StringVal (lexicographic order).
lessThan :: Primitive
lessThan [IntVal x, IntVal y] = if x < y then Right TrueVal
                                         else Right FalseVal
lessThan [StringVal x, StringVal y] = if x < y then Right TrueVal
                                               else Right FalseVal
lessThan _ = Left lessOpErr

-- Error message for lessThan
lessOpErr = "Operator '<' called with wrong numberor type of arguments"

-- Implements integer addition and string concatenation,
-- with type coercion for int to string
addOrConcat :: Primitive
addOrConcat [IntVal x, IntVal y] = Right $ IntVal (x + y)
addOrConcat [StringVal x, StringVal y] = Right $ StringVal (x ++ y)
addOrConcat [StringVal x, IntVal y] = Right $ StringVal (x ++ show y)
addOrConcat [IntVal x, StringVal y] = Right $ StringVal (show x ++ y)
addOrConcat _ = Left addOpErr

-- Error message for addOrConcat
addOpErr = "Operator '+' called with wrong number or type of arguments"

-- Implements subtraction for SubScript integers
subtractVal :: Primitive
subtractVal [IntVal x, IntVal y] = Right $ IntVal (x - y)
subtractVal _ = Left subOpErr

-- Error message for subtractVal
subOpErr = "Operator '-' called with wrong number or type of arguments"

-- Implements multiplication for SubScript integers
multiply :: Primitive
multiply [IntVal x, IntVal y] = Right $ IntVal (x * y)
multiply _ = Left mulOpErr

-- Error message for multiply
mulOpErr = "Operator '*' called with wrong number or type of arguments"

-- Implements modulo for SubScript integers
modulo :: Primitive
modulo [IntVal x, IntVal y] = Right $ IntVal (x `mod` y)
modulo _ = Left modOpErr

-- Error message for modulo
modOpErr = "Operator '%' called with wrong number or type of arguments"

-- skeleton function that was never defined as other functions
-- ended up covering the same functionality.
modifyEnv :: (Env -> Env) -> SubsM ()
modifyEnv f = undefined

putVar :: Ident -> Value -> SubsM ()
putVar name val = do
                     env <- getEnv
                     let env' = Map.insert name val env
                      in put env'

getVar :: Ident -> SubsM Value
getVar name = do
                 env <- getEnv
                 let v = Map.lookup name env
                  in case v of
                         Nothing -> fail ("var: " ++ name ++ " does not exist")
                         Just x -> return x

getFunction :: FunName -> SubsM Primitive
getFunction name = do
                      pEnv <- getPEnv
                      let f = Map.lookup name pEnv
                       in case f of
                               Nothing -> fail err
                               Just func -> return func
                      where
                       err = "func: " ++ name ++ " does not exist"

-- This functions exists to handle the special behaviour in
-- Array Comprehension. For simplicity in passing around environment
-- states it does not run in the same instance of the monad as the
-- main evaluation, but instead initiates sub-evaluations.
-- Therefore likewise all variable assignments within
-- the comprehension are temporary, and exist only within one computation.
evalArrComp :: ArrayCompr -> Context -> Either Error [Value]
evalArrComp (ACBody expr) c = case runSubsM (evalExpr expr) c of
                                   Right (v, _) -> Right [v]
                                   Left s -> Left s
--evalArrComp (ACFor i a cmp) c = case runSubExpr c a of
--                                     Left s -> Left s
--                                     Right (ArrayVal v) -> genCompr v i cmp c
--                                     Right _ -> Left aForErr
evalArrComp _ _ = Left "bad or undefined input"

-- Error message for evalArrComp case ACFor
--aForErr = "Must use array generator in Array Comprehension"

-- This function generates and evaluates a number of ArrayCompr expressions
-- ultimately producing the list of values that should be generated by the
-- comprehension and returning it.
-- genCompr :: [Value] -> Ident -> ArrayCompr
--          -> Context -> Either Error [Value]
-- genCompr arr i compr c = let xpL = map (genComprAux i compr) arr
--                           in let xpL' = map (runSubExpr c) xpL
--                               in let err = lefts xpL'
--                                   in case err of
--                                           [] -> Right (rights xpL')
--                                           (s:_) -> fail s

-- This function takes comprehension statements and
-- genComprAux :: Ident -> ArrayCompr -> Value -> Expr
-- genComprAux ident compr v = Comma (Assign ident v) (Compr compr)

evalExpr :: Expr -> SubsM Value
evalExpr (Number n) = return (IntVal n)
evalExpr (String s) = return (StringVal s)
evalExpr Undefined = return UndefinedVal
evalExpr TrueConst = return TrueVal
evalExpr FalseConst = return FalseVal
evalExpr (Var ident) = getVar ident
evalExpr (Compr arrComp) = do
                              e <- getEnv
                              pe <- getPEnv
                              case evalArrComp arrComp (e,pe) of
                                   Right vArr -> return (ArrayVal vArr)
                                   Left s -> fail s
evalExpr (Assign ident xp) = do
                                v <- evalExpr xp
                                putVar ident v
                                return v
evalExpr (Call fname xpL) = do
                               func <- getFunction fname
                               e <- getEnv
                               pe <- getPEnv
                               let xpL' = map (runSubExpr (e,pe)) xpL
                               let err = lefts xpL'
                               case err of
                                    [] -> let x = func $ rights xpL'
                                           in case x of
                                                   Right v -> return v
                                                   Left s -> fail s
                                    (s:_) -> fail s
evalExpr (Array xpL) = do
                          e <- getEnv
                          pe <- getPEnv
                          let xpL' = map (runSubExpr (e,pe)) xpL
                          let err = lefts xpL'
                          case err of
                               [] -> return (ArrayVal $ rights xpL')
                               (s:_) -> fail s
evalExpr (Comma xp1 xp2) = do
                              _ <- evalExpr xp1
                              evalExpr xp2

runSubExpr :: Context -> Expr -> Either Error Value
runSubExpr c expr = case runSubsM (evalExpr expr) c of
                    Right (v, _) -> Right v
                    Left s -> Left s

runExpr :: Expr -> Either Error Value
runExpr expr = case runSubsM (evalExpr expr) initialContext of
                    Right (v, _) -> Right v
                    Left s -> Left s
