import Test.Tasty
import Test.Tasty.HUnit

import SubsInterpreter
import SubsAst

import Control.Monad

type Error = String

main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests"
  [ testCase "intro" $
      runExpr introExpr @?= Right introResult
  , testCase "scope" $
      runExpr scopeExpr @?= Right scopeResult
  , basicEvalTests
  , advEvalTests
  , structEqTests
  , lessThanTests
  , addConcatTests
  , singleCasePrimitives
  ]

arr = ArrayVal [IntVal 1, TrueVal]
arr1 = ArrayVal [IntVal 1, TrueVal]
arr2 = ArrayVal [IntVal 1, FalseVal]
intValues = [IntVal (-10), IntVal 8]
strIntArr1 = [IntVal 5, StringVal "aaa"]
strIntArr2 = [StringVal "", IntVal 8]
strValues = [StringVal "cda", StringVal "abc"]

--Test Expr values
numVal1 = Number 5
strVal1 = String "testStr"
undefinedV = Undefined
trueV = TrueConst
falseV = FalseConst
varIdA = Var "a"
badAssign = Assign "b" varIdA
assign1 = Assign "a" numVal1
assignCommaVar = Comma assign1 varIdA
commaExp = Comma falseV trueV
funcAdd = Call "+" [numVal1, numVal1]
funcBad1 = Call "-" [trueV, numVal1]
funcBad2 = Call "*" [numVal1, varIdA]
array1 = Array [trueV, falseV, numVal1]
badArray = Array [funcBad1, trueV, badAssign]

--ArrayCompr Expr values
baseBody = Compr (ACBody trueV)
failBody = Compr (ACBody funcBad2)

--wraps structEquals and casts a result to Haskell bool
boolWrap :: ([Value] -> Either Error Value) -> [Value] -> Bool
boolWrap f x = case f x of
                    Left e -> error "bad value"
                    Right n -> if n == TrueVal then True
                                               else False

structWrap = boolWrap structEquals

lessWrap = boolWrap lessThan

--casts an IntVal to Haskell Int
intVal2Int :: Either Error Value -> Int
intVal2Int x = case x of
                    Right (IntVal n) -> n
                    _ -> error "bad value"

strVal2Str :: Either Error Value -> String
strVal2Str (Right (StringVal s)) = s
strVal2Str _ = error "bad value"

advEvalTests = testGroup "Expression Evaluation: With Comprehensions"
  [ testCase "ACBody single" $
      runExpr baseBody @?= Right (ArrayVal [TrueVal])
  , testCase "ACBody failure" $
      runExpr failBody @?= Left "var: a does not exist"
  ]

basicEvalTests = testGroup "Expression Evaluation: No Comprehensions"
  [ testCase "Return IntVal" $
      runExpr numVal1 @?= Right (IntVal 5)
  , testCase "Return StringVal" $
      runExpr strVal1 @?= Right (StringVal "testStr")
  , testCase "Return UndefinedVal" $
      runExpr undefinedV @?= Right UndefinedVal
  , testCase "Return TrueVal" $
      runExpr trueV @?= Right TrueVal
  , testCase "Return FalseVal" $
      runExpr falseV @?= Right FalseVal
  , testCase "Failed get var" $
      runExpr varIdA @?= Left "var: a does not exist"
  , testCase "Failed assign var" $
      runExpr badAssign @?= Left "var: a does not exist"
  , testCase "Assign var comma call var" $
      runExpr assignCommaVar @?= Right (IntVal 5)
  , testCase "Comma return precedence" $
      runExpr commaExp @?= Right TrueVal
  , testCase "Call func add" $
      runExpr funcAdd @?= Right (IntVal 10)
  , testCase "Failed call func subtract" $
      runExpr funcBad1 @?= Left "Operator '-' called with wrong number or type of arguments"
  , testCase "Failed expression eval in func multiply" $
      runExpr funcBad2 @?= Left "var: a does not exist"
  , testCase "Return ArrayVal" $
      runExpr array1 @?= Right (ArrayVal [TrueVal, FalseVal, IntVal 5])
  , testCase "Failed return ArrayVal" $
      runExpr badArray @?= Left "Operator '-' called with wrong number or type of arguments"
  ]
      
structEqTests = testGroup "Primitive: structural equality"
  [ testCase "equal integers" $
      structWrap [IntVal 10, IntVal 10] @?= 10 == 10
  , testCase "unequal integers" $
      structWrap [IntVal 10, IntVal 5] @?= 10 == 5
  , testCase "Equal single Value" $
      structWrap [UndefinedVal, UndefinedVal] @?= UndefinedVal == UndefinedVal
  , testCase "Unequal single Value" $
      structWrap [UndefinedVal, TrueVal] @?= UndefinedVal == TrueVal
  , testCase "Equal string" $
      structWrap [StringVal "aaa", StringVal "aaa"] @?= "aaa" == "aaa"
  , testCase "Unequal string" $
      structWrap [StringVal "abb", StringVal "aaa"] @?= "abb" == "aaa"
  , testCase "Equal array" $
      structWrap [arr, arr] @?= arr == arr
  , testCase "Unequal array" $
      structWrap [arr1, arr2] @?= arr1 == arr2
  ]

lessThanTests = testGroup "Primitive: less than"
  [ testCase "integers" $
      lessWrap intValues @?= (-10) < 8
  , testCase "strings" $
      lessWrap [StringVal "abc", StringVal "cda"] @?= "abc" < "cda"
  ]
  
addConcatTests = testGroup "Primitive: add and concat operator"
  [ testCase "integers" $
      intVal2Int (addOrConcat intValues) @?= ((-10) + 8)
  , testCase "integer and string" $
      strVal2Str (addOrConcat strIntArr1) @?= "5aaa"
  , testCase "string and integer" $
      strVal2Str (addOrConcat strIntArr2) @?= "8"
  , testCase "strings" $
      strVal2Str (addOrConcat strValues) @?= "cdaabc"
  ]
  
singleCasePrimitives = testGroup "Primitives: single case primitives"
  [ testCase "subtraction" $
      intVal2Int (subtractVal intValues) @?= ((-10) - 8)
  , testCase "multiplication" $
      intVal2Int (multiply intValues) @?= ((-10) * 8)
  , testCase "modulo" $
      intVal2Int (modulo intValues) @?= ((-10) `mod` 8)
  ]
  
introExpr :: Expr
introExpr =
  Comma (Assign "xs"
          (Array [Number 0, Number 1, Number 2, Number 3, Number 4,
                  Number 5, Number 6, Number 7, Number 8, Number 9]))
   (Comma (Assign "squares"
            (Compr (ACFor "x" (Var "xs")
                     (ACBody (Call "*" [Var "x",Var "x"])))))
     (Comma (Assign "evens"
              (Compr (ACFor "x" (Var "xs")
                       (ACIf (Call "===" [Call "%" [Var "x", Number 2],
                                          Number 0])
                         (ACBody (Var "x"))))))
       (Comma (Assign "many_a"
                (Compr (ACFor "x" (Var "xs")
                         (ACFor "y" (Var "xs")
                           (ACBody (String "a"))))))
         (Comma (Assign "hundred"
                  (Compr (ACFor "i" (Array [Number 0])
                           (ACFor "x" (Call "Array" [Number 5])
                             (ACFor "y" (Call "Array" [Number 20])
                               (ACBody (Assign "i"
                                         (Call "+" [Var "i", Number 1]))))))))
           (Array [Var "xs", Var "squares", Var "evens",
                   Var "many_a", Var "hundred"])))))

introResult :: Value
introResult = ArrayVal
  [ ArrayVal [IntVal n | n <- [0..9]]
  , undefined
  , undefined
  , undefined
  , undefined
  ]

scopeExpr :: Expr
scopeExpr =
  Comma (Assign "x" (Number 42))
   (Comma (Assign "y" (Compr (ACFor "x" (String "abc")
                               (ACBody (Var "x")))))
     (Array [Var "x", Var "y"]))

scopeResult :: Value
scopeResult = ArrayVal
  undefined
